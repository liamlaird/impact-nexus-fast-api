from fastapi import FastAPI
from fastapi.responses import FileResponse
from docx import Document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt

app = FastAPI()

@app.get("/")
async def root():
    # Open the template document
    document = Document('Article8C.docx')
    
    # Find the paragraph containing the placeholder and replace it with the appropriate value
    for paragraph in document.paragraphs:
        if 'PRODUCT_NAME' in paragraph.text:
            paragraph.text = paragraph.text.replace('PRODUCT_NAME', 'IMPACT NEXUS DASHBOARD')
        if 'LEGAL_ENTITY_ID' in paragraph.text:
            paragraph.text = paragraph.text.replace('LEGAL_ENTITY_ID', '12345')

    ## Replace with bullets (Not working)
    if 'PERFORMANCE_OF_SUSTAINABILITY_INDICATORS':
        bullet_points = ['Excellent', 'Performance', 'All', 'Round']
        new_p = document.add_paragraph(style='List Bullet')
        new_p.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
        new_p.style.font.size = Pt(12)
        for item in bullet_points:
            new_p.add_run(item + '\n')

    # Save the filled-out document as a new file
    document.save('Article8C DONE.docx')

    # Return the filled-out document as a response
    return FileResponse('Article8C DONE.docx')
