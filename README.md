# impact-nexus-fast-api

# SETUP
sudo apt update
sudo apt install uvicorn python3 python3-pip
pip install -r requirements.txt

# RUN LOCALLY
uvicorn main:app --reload

# CALL FROM BROWSER
http://127.0.0.1:8000